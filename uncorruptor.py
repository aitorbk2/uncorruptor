import os,time,hashlib,zlib,sys,datetime #https://mborgerson.com/creating-an-executable-from-a-python-script
import sqlite3 as lite
from _codecs import encode

def md5String(String):
    hash_md5 = hashlib.md5()
    hash_md5.update(String)
    return hash_md5

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5


def sha256(fname):
    hash_sha256 = hashlib.sha256()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_sha256.update(chunk)
    return hash_sha256


def adler32(fname):
    asum=1
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(32768), b""):
            asum=zlib.adler32(chunk,asum)
    return asum

def crc32(fname):
    asum=1
    try:
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                asum=zlib.crc32(chunk,asum)
    except:
        asum=0
    return asum


def existe(hashRutaNumbre,hashContenido,fechaModificacion,fileSize,tipoHash,cur,name):
    resultado=0 #0=error/not found, 1=date differs so does content, update, 2=same date, content differs, error, 3=hash is the same, same date, don't update, 4=hash is the same, different date
    try:
        cur.execute("select hashRutaNumbre,filename,hashContenido,fechaModificacion,fechaTratamiento,fileSize,tipoHash from ficheros where hashRutaNumbre=? ",(hashRutaNumbre,))
        row = cur.fetchone()
        if row is None:
            #print('No rows with hash  %s'%hashRutaNumbre)
            resultado=0
        else:
            #print("Encontrada fila", row[1],"-",row[2],"-", row[3],"-Fecha llega->",fechaModificacion)
            if fechaModificacion == row[3]: #We know that the date is the same: lets compare checksum of content.
                #print("Fecha coincide")
                if hashContenido==row[2]:
                    resultado=3
                    #print("Contenido coincidente")
                else:
                    resultado=2
                    #print("Contenido no coincidente")
            else:
                resultado=2
                if hashContenido==row[2]:
                #print("Else fecha")
                    resultado=4
        
    except Exception as e:
        resultado=0
        print ("Error fetch  %s:" % e.args[0])
    return resultado
    
numFiles=0
numFiles0=0
numFiles1=0
numFiles2=0
numFiles3=0
numFiles4=0
numFilesPermDenied=0
numFilesNotFound=0
numFilesError=0
t0 = time.perf_counter()
hashType=2

#TODO obtener fecha de última comprobación general (BBDD)

con = None

try:
    con = lite.connect('C:/desarrollo/sqlite/aitor.db')
    cur = con.cursor()    
    con.execute("PRAGMA page_size = 65536")
    #con.execute("PRAGMA journal_mode = WAL")
    #con.execute("PRAGMA wal_autocheckpoint = 16")
    #con.execute("PRAGMA journal_size_limit = 1536")
    con.execute("PRAGMA temp_store = MEMORY")
    con.execute("VACUUM")
    #con.execute("drop table test")    
    #con.execute('CREATE TABLE ficheros(hashRutaNumbre numeric PRIMARY KEY, filename text, hashContenido text, fechaModificacion,fechaTratamiento,fileSize,tipoHash  )')
    #cur.execute('SELECT SQLITE_VERSION()')
    #data = cur.fetchone()
    #print ("SQLite version: %s" % data)   
    #cur.execute('CREATE TABLE FICHEROS ("hashRutaFichero" VC2(10) PRIMARY KEY ,"hashRuta" VC2(10) NOT NULL, "hashContenido" VC2(10), "fechaModificacion" DATETIME ,"tamaño" NUMBER NOT NULL, "tipoHash" NUMBER NOT NULL)')
                    
    #for root, dirs, files in os.walk("D:/basura_a_borrar/temp", topdown=False):
    for root, dirs, files in os.walk("D:/basura_a_borrar/temp/boa", topdown=False):
    #for root, dirs, files in os.walk("c:/Program Files", topdown=False):
    #for root, dirs, files in os.walk("c:/", topdown=False):
        numLocalFiles = 0
        for name in files:
            numFiles=numFiles+1
            numLocalFiles=numLocalFiles+1
            try:
                fileStat=os.stat(os.path.join(root, name))
                fileSize=fileStat.st_size
                fileDate=fileStat.st_mtime
                fileHash=md5(os.path.join(root, name))
                hashRutaFichero=md5String(root.encode(encoding='utf_8', errors='strict'))
                pathFilenamehash=md5String(root.encode(encoding='utf_8', errors='strict')+name.encode(encoding='utf_8', errors='strict'))              
                #print(str(numFiles) + " " + str(numLocalFiles) + " " + os.path.join(root, name) + " " +  name[0] + " Filesize:" + str(fileSize)  + " FileDate:" + str(fileDate)+ " FileHash:" + str(fileHash.hexdigest()) )
                #con.execute('insert into FICHEROS (pathFilenamehash, pathHash, contentHash, modDate, fileSize, hashType) values (12,12,?,12,12,12)',"1")
                digestReal=int(pathFilenamehash.hexdigest(),16)
                hashRutaNumbre=pathFilenamehash.hexdigest()
                hashContenido=fileHash.hexdigest()
                tipoHash=2
                fechaModificacion=fileDate
                cambio=existe(hashRutaNumbre,hashContenido,fechaModificacion,fileSize,tipoHash,cur,name)
                #http://www.pydanny.com/why-doesnt-python-have-switch-case.html*
                #0=error/not found, 1=date differs so does content, update, 2=same date, content differs, error, 3=hash is the same, same date, don't update, 4=hash is the same, different date
                #print ("Y lo que tenemos en cambio es ->",cambio,"<-")
                if cambio == 0:
                    con.execute("insert into ficheros (hashRutaNumbre,filename,hashContenido,fechaModificacion,fechaTratamiento,fileSize,tipoHash) values (?,?,?,?,?,?,?)",(pathFilenamehash.hexdigest(), name,fileHash.hexdigest(),fileDate,time.time(),fileSize,"2",))
                    numFiles0=numFiles0+1
                elif cambio == 1:
                    con.execute("update ficheros set hashContenido=? ,fechaModificacion=?,fechaTratamiento=?,fileSize=? WHERE hashRutaNumbre=? " ,(hashContenido,fechaModificacion,time.time(),fileSize,hashRutaNumbre,))
                    numFiles1=numFiles1+1
                elif cambio == 2:
                    numFiles2=numFiles2+1
                    print("Corrupted File! :  os.path.join(root, name).  %s Corrupted Files so far",str(numFiles2))
                elif cambio == 3:
                    con.execute("update ficheros set fechaTratamiento=? WHERE hashRutaNumbre=? " ,(time.time(),hashRutaNumbre,))
                    numFiles3=numFiles3+1
                else:
                    con.execute("update ficheros set fechaTratamiento=? WHERE hashRutaNumbre=? " ,(time.time(),hashRutaNumbre,))
                    numFiles4=numFiles4+1
                #c.execute('insert into FICHEROS (hashRutaFichero, hashRuta, hashContenido, fechaModificacion, tamaño, tipoHash) values (12,12,12,' + str(fileHash.hexdigest())+ '12,12,12)')
                #con.commit()
            except lite.Error as e:
                print ("Error SQLite %s:" % e.args[0] + " " + fileHash.hexdigest() + os.path.join(root, name))
                numFilesError=numFilesError+1 
            except  Exception as e:
                #nothing to see here, we should count the failed to read files and add to collection
                
                #print ("Error normal %s:" % e.args[0] + fileHash.hexdigest() + os.path.join(root, name))
                numFilesError=numFilesError+1
                if e.args[0] == 13:
                    numFilesPermDenied=numFilesPermDenied+1 
                elif e.args[0] == 2:
                    numFilesNotFound=numFilesNotFound+1
                else:
                    print ("Error %s %s:" % e.args[0] %e.args[1]  + os.path.join(root, name))
    t1 = time.perf_counter()
    #print("%.3f Totalseconds" % (t1-t0) + "Files:" + str(numFiles))
    print("%.3f Totalseconds" % (t1-t0) + " Total Files:" + str(numFiles)+ " New:" + str(numFiles0)+ " Updated:" + str(numFiles1)+ " Corrupted:" + str(numFiles2)+ " Check OK:" + str(numFiles3)+ " Date Changed:" + str(numFiles4)+ " Permission Denied:" + str(numFilesPermDenied)+ " Not found:" + str(numFilesNotFound))
    con.commit()
    con.close()
    
except lite.Error as e:
    print ("Error %s:" % e.args[0])
    #print ("Error ")
    sys.exit(1)
    
finally:
    
    if con:
        con.close()